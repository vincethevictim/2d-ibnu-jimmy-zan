package crappyBird;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Game extends JPanel{
	
	static int HEIGHT = 800;						
	static int WIDTH = 600;							
	BirdMan birdy = new BirdMan();					
	Wall wall = new Wall(WIDTH);					
	Wall wall2 = new Wall(WIDTH + (WIDTH / 2));		
	static int score = 0;							
	int scrollX = 0;								
	static boolean dead = false;					
	static String deathMessage = "" ; 				
	
	
	BufferedImage img = null;{
	try {
		img = ImageIO.read(new URL("http://i.imgur.com/cXaR0vS.png"));
	} catch (IOException e) {
		System.out.println("WRONG BACKGROUND");		
	}}
	
	
	public Game(){
		
		
		this.addMouseListener(new MouseAdapter(){

			public void mousePressed(MouseEvent arg0) {
				birdy.jump();
			}
		
		});	
		
	}

	@SuppressWarnings("static-access")
	public void paint(Graphics g){
		super.paint(g);
		
		g.drawImage(img, scrollX, 0, null);								
		g.drawImage(img, scrollX + 1800, 0, null);			
		
		wall.paint(g);			
		wall2.paint(g);			
 		birdy.paint(g);			
 	
 		g.setFont(new Font("comicsans", Font.BOLD, 40));
 		g.drawString("" + score, WIDTH / 2 - 20, 700);
 		g.drawString(deathMessage, 200, 200);				
	}
	
	@SuppressWarnings("static-access")
	public void move(){

		wall.move();			
		wall2.move();			
		birdy.move();			
	
		scrollX += Wall.speed;	
		
		if (scrollX == -1800)	
			scrollX = 0;
		
		if (dead){				
			wall.x = 600;
			wall2.x = 600 + (WIDTH / 2);
			dead = false;
		}
		
		if ( (wall.x == BirdMan.X) || (wall2.x == BirdMan.X) ) 	
			score();
	}
	
	public static void score(){
		score += 1;
	}
	
}