package crappyBird;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;



public class Wall {

	Random rnd = new Random();						
		
	int x ;											
	int y = rnd.nextInt(Game.HEIGHT - 400) + 200;	
	static int speed = - 6;							
	int WIDTH = 45;									 
	int height = Game.HEIGHT - y;					
	int GAP = 200;									
	
	//procures the Wall image from Imgur
	static BufferedImage img = null;{
            try {
                img = ImageIO.read(new File("src/resources/tiang.png")); //prints "WRONG WALL" if there's trouble with Imgur
            } catch (IOException ex) {
                Logger.getLogger(Wall.class.getName()).log(Level.SEVERE, null, ex);
            }
}
	
	public Wall(int i){								//allows me to differentiate the x positions of the two walls
		this.x = i;
	}
	
	//draws the wall
	public void paint(Graphics g){
		g.drawImage(img, x, y, null);								//top part 
		g.drawImage(img, x, ( -Game.HEIGHT ) + ( y - GAP), null);	//bottom part
	}
	
	public void move(){
		
			x += speed;								//scrolls the wall
	
		//These Rectanlges are used to detect collisions
		Rectangle wallBounds = new Rectangle(x, y, WIDTH, height);
		Rectangle wallBoundsTop = new Rectangle(x, 0, WIDTH, Game.HEIGHT - (height + GAP));
		
		//If birdman collids with a wall, he dies and  the game, bird, and walls are all reset
		if ( (wallBounds.intersects(BirdMan.getBounds()) ) || (wallBoundsTop.intersects(BirdMan.getBounds()))){
			BirdMan.reset();
			died();
		}
			
		//pushes the wall back to just off screen on the right when it gets offscreen on the left (the loop)
		if (x <= 0 - WIDTH){
			x = Game.WIDTH;
			y = rnd.nextInt(Game.HEIGHT - 400) + 200;
			height = Game.HEIGHT - y;
		}		
	}
	

	//this is executed on death, just sets a random y value and tells Game that the bird died :(
	public void died(){
			y = rnd.nextInt(Game.HEIGHT - 400) + 200;
			height = Game.HEIGHT - y;
			Game.dead = true;
	}
}